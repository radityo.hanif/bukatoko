from database import *
from flask import Flask, request, jsonify, make_response
import requests
import json

app = Flask(__name__)

@app.route("/", methods=["GET"])
def index():
    return "welcome to server 2"

@app.route("/produk/<id>", methods=["GET"])
def getProduk(id):
    sql = f"SELECT * FROM produk WHERE produk.id = {id}"
    data = getData(query=sql)
    if data != None:
        return make_response(
            jsonify({
                "status" : 200,
                "message" : f"query berhasil, id produk {id} terdaftar",
                "result" : {
                    "id_penjual" : data[0][1],
                    "harga" : data[0][2],
                    "berat" : data[0][3],
                    "nama_produk" : data[0][4],
                    "gambar_produk" : data[0][5],
                    "diskon_produk" : data[0][6],
                    "promo_produk" : data[0][7],
                    "stok" : data[0][8],
                    "deskripsi" : data[0][9]
                }
            })
        )
    else:
        return make_response(
            jsonify({
                "status" : 404,
                "message" : f"query gagal, id produk {id} tidak terdaftar"
            })
        )

@app.route("/produk/<id>", methods=["POST"])
def insertProduk(id):
    hasil = {"status": "gagal insert data produk"}
	
	try:
		data = request.json

		query = "INSERT INTO tb_siswa(id_penjual, harga, berat, nama, gambar, id_diskon, id_promo, stok, deskripsi) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s)"
		values = (data["id_penjual"], data["harga"], data["berat"], data["nama"], data["gambar"], data["id_diskon"], data["id_promo"], data["stok"], data["deskripsi"])
		hasil = {"status": "berhasil insert data siswa"}

	except Exception as e:
		print("Error: " + str(e))

	return jsonify(hasil)

@app.route("/produk/<id>", methods=["PUT"])
def updateProduk(id):
    hasil = {"status": "gagal update data produk"}
	
	try:
		data = request.json
		id_awal = data["id_awal"]

		query = "UPDATE tb_produk SET id = %s "
		values = (id_awal, )

		if "id_ubah" in data:
			query += ", id = %s"
			values += (data["id_ubah"], )
		if "id_penjual" in data:
			query += ", id_penjual = %s"
			values += (data["id_penjual"], )
		if "harga" in data:
			query += ", harga = %s"
			values += (data["harga"], )
		if "berat" in data:
			query += ", berat = %s"
			values += (data["berat"], )
        if "nama" in data:
			query += ", nama = %s"
			values += (data["nama"], )
        if "gambar" in data:
			query += ", gambar = %s"
			values += (data["gambar"], )
        if "id_diskon" in data:
			query += ", id_diskon = %s"
			values += (data["id_diskon"], )
        if "id_promo" in data:
			query += ", id_promo = %s"
			values += (data["id_promo"], )
        if "stok" in data:
			query += ", stok = %s"
			values += (data["stok"], )
        if "deskripsi" in data:
			query += ", deskripsi = %s"
			values += (data["deskripsi"], )

		query += " WHERE id = %s"
		values += (id_awal, )

		hasil = {"status": "berhasil update data produk"}

	except Exception as e:
		print("Error: " + str(e))

	return jsonify(hasil)

@app.route("/produk/<id>", methods=["DELETE"])
def deleteProduk(id):
    hasil = {"status": "gagal hapus data produk"}
	
	try:
		data = request.json

		query = "DELETE FROM tb_produk WHERE id=%s"
		values = (id,)
		hasil = {"status": "berhasil hapus data produk"}

	except Exception as e:
		print("Error: " + str(e))

	return jsonify(hasil)
""" @app.route("/penjual/<id>", methods=["GET"])
def getKotaPenjual(id):
    sql = f"SELECT id_kota FROM `penjual` WHERE penjual.id = {id}"
    data = getData(query=sql)
    if data != None:
        return make_response(
            jsonify({
                "status" : 200,
                "message" : f"query berhasil, id penjual {id} terdaftar",
                "result" : {
                    "id_kota" : data[0][0]
                }
            })
        )
    else:
        return make_response(
            jsonify({
                "status" : 404,
                "message" : f"query gagal, id penjual {id} tidak terdaftar"
            })
        )

@app.route("/product", methods=["GET"])
def getAllProduct():
    sql = f"SELECT a.nama, a.harga, a.stok, count(c.id_produk) total_pembelian, b.nama, a.id_penjual FROM produk a left join pembelian c on a.id=c.id_produk LEFT JOIN penjual b on a.id_penjual=b.id group by a.id, c.id_produk"
    data = getData(query=sql)
    data_string = json.dumps(data)
    return data_string

@app.route("/product/<namaProduk>", methods=["GET"])
def getProductByName(namaProduk):
    print(namaProduk)
    sql = f"SELECT a.nama nama_produk, a.harga, a.stok, count(c.id_produk) total_pembelian, b.nama nama_penjual, a.id_penjual FROM produk a left join pembelian c on a.id=c.id_produk LEFT JOIN penjual b on a.id_penjual=b.id where a.nama LIKE '%{namaProduk}%' group by a.id, c.id_produk;"
    data = getData(query=sql)
    data_string = json.dumps(data)
    return data_string




if __name__ == "__main__":
    app.run(port=5002, debug=True)