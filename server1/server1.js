// import module
const http = require('http');
const bodyParser = require("body-parser");
let request = require('request');

// setup app
const express = require("express");
const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
//

// setup morgan untuk log
const morgan = require('morgan');
app.use(morgan('dev'));
// 

// Variabel untuk port
const port = process.env.PORT || 5001;

// api key
api_key = "34943098462755445d31e41f9fa7a8db"



// Endpoint
app.get('/', (req, res, next) => {
    res.json({
        message:"Welcome to server 1"
    });
});

app.post('/ongkir', (req, res, next) => {
    let dataPost = req.body;
    let dataAllowed = [
        "origin",
        "destination",
        "weight",
        "courier",
        "service"
    ];

    let dataPostEmpty= [];
    let dataPostValid= {};

    dataAllowed.forEach((key) => {
        console.log(key)
        if(key in dataPost){
            if (dataPost[key] == ""){
                dataPostEmpty.push(key);
            } else {
                dataPostValid[key] = dataPost[key];
            }
        } else {
            dataPostEmpty.push(key);
        }
    });

    // Validasi form yang belum diisi
    if(dataPostEmpty.length != 0 ){
        res.status(400).json({
            status: 400,
            message: "Beberapa form data belum diisi",
            empty_form: dataPostEmpty
        });     
    } else {
        // Request data ke API RajaOngkir
        const options = {
            'method': 'POST',
            'url': 'https://api.rajaongkir.com/starter/cost?=',
            'headers': {
                'key': api_key,
                'Content-Type': 'application/json'
            },
            'body': JSON.stringify(dataPostValid)
        };
        request(options, (error, response, body) => {
            dataRajaOngkir = JSON.parse(body)

            // Validasi keberhasilan request data ke API RajaOngkir
            if (error) throw new Error(error);
            serviceList = [];
            if(dataRajaOngkir["rajaongkir"]["status"]["description"] == "OK"){
                dataRajaOngkir["rajaongkir"]["results"][0]["costs"].forEach((data) => {
                    serviceList.push(data["service"].toLowerCase());
                    if (data["service"].toLowerCase() == dataPostValid["service"].toLowerCase()){
                        let dataCost = data;
                    }
                });

                // Jika service  tersedia
                if (dataPostValid["service"] in serviceList){
                    res.status(200).json({
                        status : 200,
                        result : {
                            "kurir" : dataPostValid["courier"],
                            "paket_pengiriman" : dataCost["description"],
                            "ongkos_kirim" : dataCost["cost"][0]["value"],
                            "estimasi_sampai" : dataCost["cost"][0]["etd"]
                        }
                    });
                } else { // Jika service tidak tersedia
                    res.status(404).json({
                        status : 404,
                        available_service : serviceList,
                        message : `paket pengiriman ${dataPostValid['service']} tidak tersedia pada kurir ${dataPostValid['courier']}`
                    });
                }
            }
        });
    }
});

// Error Handling saat tidak ada route yang ter setup
app.use((req, res, next) => {
    const error = new Error('API tidak ditemukan');
    error.status = 404;
    next(error);
});
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message : error.message
        }
    });
});
// 

// Membuat sebuah server dan menjalankan server dengan fungsi listen();
const server = http.createServer(app)
app.listen(port);
console.log(`Server1 running on http://localhost:${port}`);